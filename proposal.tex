\documentclass[11pt]{article}

\usepackage{graphicx}
\usepackage{bm}
\usepackage{amsmath}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows, positioning}

\begin{document}

% Define block styles
\tikzstyle{decision} = [diamond, draw, fill=blue!20, 
    text width=4.5em, text badly centered, node distance=3cm, inner sep=0pt]
\tikzstyle{block} = [rectangle, draw, fill=blue!20, 
    text width=5em, text centered, rounded corners, minimum height=4em]
\tikzstyle{line} = [draw, -latex']
\tikzstyle{cloud} = [draw, ellipse,fill=red!20, node distance=3cm,
    minimum height=2em]
    

\title{Turbomachinery gradient-based design optimisation using multi-disciplinary adjoint sensitivities}
\author{Pavanakumar Mohanamuraly}
\maketitle

\section{Project summary (max 3000 words)}
Turbomachines are amongst the most efficient power generation and energy conversion devices for their given class of operations. Use of turbines goes beyond traditional fossil fuels into renewable sources like wind and tidal energy. In fact, turbochargers are one of the fastest growing markets in Asia with estimated $18.49B$ USD by 2021. Agricultural tractors are predicted to be one of the largest application in off-highway turbochargers. Even in 2018, Agriculture in India accounts for $17\%$ of the national GDP. The country also has a fast-growing automobile sector, where turbochargers are essential to improve fuel efficiency and control emission of pollutant gases. Traditionally, innovation in Asia has focused on low-cost production and manufacturing practices. The industries are now gearing to shift to innovation in design and technology.

Affordable computer power makes simulation-based design feasible. It brings new opportunities since single discipline empirical optimisation have reached saturation. Until recently, gradient-free methods and Design-of-Experiments (DOE) were predominantly used in the optimisation process. With the advent of adjoint algorithmic differentiation (AD), gradients can be easily and efficiently obtained from simulation codes. This also opens the possibility to combine gradients from multiple disciples. Combining the cheap multi-disciplinary gradients using AD and efficient gradient-based optimisation methods leads to the possibility of many innovative and intuitive designs, for example, the BatMoWheel shown in figure \ref{fig:matbowheel}. Combining the optimisation directly to the Computer Aided Design (CAD) model enables quick turn around time from design to manufacturing.

\begin{figure}
    \includegraphics[scale=0.2]{figures/BMW}
    \includegraphics[scale=0.2]{figures/trad}
\caption{Comparison of an innovative turbocharger wheel design BMW-6.4L-2T (left) vs a traditional generic wheel (right).}
\label{fig:matbowheel}
\end{figure}

NutsCFD\cite{kumarThesis18} is an open-source CFD solver with a robust adjoint capability and fully validated for turbo-machinery applications. The solver was co-developed by the author during his PhD at the Queen Mary University of London. NutsCFD has been used successfully in industrial projects involving single disciplinary optimisation, namely aerodynamic design. 

We propose to develop a structural solver based on linear Finite Element Method and couple it with the edge-based second order finite volume spatial discretisation of NutsCFD. Source transformation AD is used to obtain the gradients (from both disciples). The nodal degrees-of-freedom between the aerodynamic and structural disciplines can be linked using suitable interpolation. Moreover, the implicit Jacobian of the aerodynamic and structural solver can be combined into one matrix structure. This makes the quasi-Newton approach in NutsCFD even more robust and less computationally expensive for multi-disciplinary optimisation problems. The open-source licensing of NutsCFD comes with a zero licensing cost. This makes it attractive for the industry to adopt the code for their design and analysis.


\section{Objectives of project (max 1500 characters)}
The main objective of the project is to build a framework for the multi-disciplinary adjoint optimisation of Turbo-machinery blades using adjoint sensitivities. This requires the development of a structural solver based on linear Finite Element Method and its adjoint sensitivity. This is then coupled to the open-source NutsCFD aerodynamic solver. Finally, a gradeint-based optimisation workflow using parametric CAD for the blade shape is setup using the combined adjoint sensitivities.

\section{Expected output and outcome of the proposal (max 1500 characters)}
\begin{itemize}
	\item The structural linear FEM solver and its adjoint (made open-source as part of NutsCFD)
    \item Parametric blade modeller to generate the CAD model of the blade
	\item Mulit-disclipinary Optimisation framework demonstration test cases
	\begin{itemize}
	  \item Generic radial compressor\cite{Eisenlohr2002}
    \end{itemize}
	\item Publications from the optimisation results
\end{itemize}

\section{Work Methodology and Research plan (not more than 3 pages)}
\subsection{Background and Literature review}
Gradient-based optimisation using adjoint sensitivity has become a popular method for aerodynamics shape design problems\cite{Pironneau1974} \cite{Jameson1988}. A single discipline optimisation like aerodynamic design disregards important constraints like stresses. For example, in compressor blade design the stresses due to centrifugal and aerodynamics pressure loads are two important design constraints. In order to include these constraints, one requires a coupled fluid-structure approach to design. Mostly such multi-disciplinary optimisation have been performed using using gradient-free methods\cite{Joly2014}\cite{Mueller2013}. Gradient-free methods are prohibitively expensive, when the cost of analysis is not negligible. Adjoint methods enable evaluation of gradients at a cost independent of the number of design variables and dependent on the number of output variables. 
In the present work, we use gradient-based optimisation with the necessary gradients obtained using adjoint method. We use the discrete adjoint approach to calculate the gradients, since automatic generation of adjoint code is possible using Algorithmic Differentiation\cite{Griewank08} (when the primal source code is available).

Another key aspect of shape optimisation is the recovery of the CAD model from the final optimised shape. Often approximations are employed in CAD-free optimisation to convert the final optimised shape to CAD. In addition, manufacturing constraints are difficult to impose in a CAD-free context. For example, in radial compressors the blades should be constrained to be ruled surfaces for Flank milling process\cite{Davim2012}. Therefore, we parametrise the given blade parameters like blade angle, thickness, meridional hub/shroud, etc using Bezier curves \cite{Verstraete2010}. The surface is then evolved by sweeping the 2d curves into 3d Bezier surfaces\cite{Piegl2012}. Thus, we introduce the necessary geometric constraints like minimum thickness, leading and training edge radius in the parametrisation itself\cite{Mueller2017}. For example, one can constraint the sweeping path curve to be linear to maintain the manufacturing constraints of ruled surface. The parametric CAD kernel can be easily differentiated using AD tools\cite{Banovic2018}\cite{Orest2017}. Using the differentiated CAD it is then possible to obtain the design parameter sensitivity to variations of the surface coordinates of the blade. This is referred to as the CAD sensitivity.

\subsection{CAD parametrisation}
The approach of CADO\cite{Verstraete2010} is followed for the parametrisation of the radial compressor blade. Here the blade is defined using four parameters,
\begin{itemize}
	\item Meridional contour at hub and shroud (figure \ref{fig:meridional})
	\item Camber line of the main blade (defined using blade angle distribution shown in figure \ref{fig:beta})
	\item Thickness distribution normal to camber line (figure \ref{fig:thickness})
	\item Total number of blades
\end{itemize}
The first and third parameters are modelled using Bezier curves with specified control points. The coordinates of the control points become the design parameters of the blade, whose position is perturbed by the optimiser to minimise the given cost function. The camber line is specified using the blade angle distribution ($\beta$), modelled using a Bezier curve. $\beta$ is defined as the angle between the meridional plane $m$ and the streamline $S$ as shown in figure \ref{fig:beta}. The value of $\beta_0$ and $\beta_3$ correspond to the inlet and outlet blade angles. The actual $x,y,z$ position of the camber line can be obtained from $\beta$ using the following relation,
\begin{equation}
	R d\theta = dm \tan{\beta}
\end{equation}
The lean or rake of the blade is not considered in the present parametrisation.
\begin{figure}
	\includegraphics[scale=0.8]{figures/meridonal}
	\caption{Meridional plane hub and shroud Bezier curve parametrisation}
    \label{fig:meridional}
\end{figure}
\begin{figure}
	\includegraphics[scale=0.8]{figures/beta_camber}
	\includegraphics[scale=0.8]{figures/beta_dist}
	\caption{Blade angle $\beta$ distribution, reference to meridional plane $m$ from streamline $S$}
    \label{fig:beta}
\end{figure}
\begin{figure}
	\includegraphics{figures/thickness}
	\caption{Blade thickness distribution as Bezier curve; control points shown in red}
	\label{fig:thickness}
\end{figure}

\subsection{Linear elasticity solver and its adjoint}
The standard Galerkin formulation of linear elastic equations in weak form shown in equations~\eqref{eqn:weak}-\eqref{eqn:stiff} is used to solve for stresses in the radial compressor. The element stiffness matrix $\mathbf{K}^e$ is discretised using the $8$-node brick element\cite{Zienkiewicz2005} for quick turn around time. Note that the $\mathbf{f_c}$ is the centrifugal force term due to blade rotation. The term $\mathbf{B}$ is the strain displacement matrix and $\mathbf{E}$ is the elastic moduli matrix. We will restrict to isoparametric solid elements with three translational degrees of freedom (DOF) per node in this work.
\begin{eqnarray}
	\mathbf{K} \mathbf{u} &=& \mathbf{f} + \mathbf{f_c} \label{eqn:weak}\\
	\mathbf{K}^e &=& \int \limits_{\Omega^e} \mathbf{B}^T \label{eqn:stiff}\mathbf{E} \mathbf{B} d\Omega \approx \sum_i \sum_j \sum_k w_{ijk} \mathbf{B}_{ijk}^T \mathbf{E} \mathbf{B}_{ijk} J_{ijk}
\end{eqnarray}
The stress $\bm{\sigma}$ can be recovered from the displacement $\mathbf{u}$ using the relation in equation~\eqref{eqn:stress_recover}. The criteria that the von Mises stress $\sigma_{Mises}$ is not greater than the yield stress $\sigma_{yield}$ , shown in equation~\eqref{eqn:mises_crit} is used as a structural constraint for the optimisation problem. Hence, the sensitivity of the constraint must be calculated using the adjoint formulation of the problem. In order to differentiate this function we convert the inequality to an equality constraint as shown in equation~\eqref{eqn:mises_modify}.
\begin{equation}
	\bm{\sigma} = \mathbf{E}\mathbf{B}\mathbf{u}
	\label{eqn:stress_recover}
\end{equation}
\begin{equation}
	\mathbf{\sigma}_{Mises} = \sqrt{ \frac{3}{2} \sigma_{ij} \sigma_{ij} - \frac{1}{2} \sigma_{kk}^2} \not> \sigma_{yield}
	\label{eqn:mises_crit}
\end{equation}
\begin{equation}
	J_{mech} = \left(\frac{\sigma_{Mises}}{\sigma_{yield}} - 1 \right), \hspace{5pt}  -\infty < J_{mech} < 0
	\label{eqn:mises_modify}
\end{equation}
The cost-function $J_{mech}$ in equation~\eqref{eqn:mises_modify} is differentiated in the adjoint mode to yield the right hand side source term of the structural adjoint equation~\eqref{eqn:st_adjoint}. Note that $\mathbf{\overline{K}}$ is the adjoint stiffness matrix obtained using adjoint AD of the discrete form of equation \eqref{eqn:stiff} and $\mathbf{v}$ is the adjoint displacement. The adjoint sensitivity for this cost-function is assembled using the adjoint solution of equation~\eqref{eqn:st_adjoint}.
\begin{equation}
	\mathbf{\overline{K}} \mathbf{v} = \frac{\partial J_{mech}}{\partial \mathbf{u}}
	\label{eqn:st_adjoint}
\end{equation}

\subsection{Optimisation methodology}
The SLSQP\cite{Wright1999} is a robust method for optimisation problems with many constraints. In addition to the bounds, this method requires the gradient of the constraint. This gradient can be efficiently obtained using the adjoint method described previously. The overall optimisation process is shown in figure \ref{fig:timeline}. Here, the von Mises cost-function $J_{mech}$ and its gradient are an extra constraint to the aerodynamic shape optimisation of the radial compressor\cite{Eisenlohr2002}. The main cost function $\mathbf{J}$ for the shape optimisation is the total-to-total efficiency at defined operating points of the compressor speed line. After successful run of preliminary  optimisation, additional constraints can be added like mass flow and total pressure ratio. NutsCFD already calculates the necessary adjoint and assembles the required gradients\cite{kumarThesis18}. A robust mesh deformation module is also available, which deforms the mesh when the parametrisation is updated after each optimisation step. The final optimised shape can be extracted as a CAD model from the parametrisation kernel.
\begin{figure}
\includegraphics{figures/flow_chart}
\caption{Optimisation flow chart}
\label{fig:flowchart}
\end{figure}

\begin{figure}[!h]
	\includegraphics[scale=0.6]{figures/timeline}
	\caption{Time line of the research project}
	\label{fig:timeline}
\end{figure}

\bibliographystyle{abbrv}
{\footnotesize
\bibliography{sample.bib}}

\end{document}


%Design sensitivity information is an essential part of gradient-based optimisation. By definition, gradient here is a measure of the sensitivity of the design parameter to infinitesimal change in the values of the cost-function (minimised by the optimiser). The adjoint approach to compute the sensitivities is an effective approach if there are many design variables and the computational cost of a single analysis is not negligible. Many commercial and open-source \cite{Palacios2013}\cite{Othmer2006} solver now support adjoints. Most of the aforementioned solvers are based on the continuous adjoint approach, where the adjoint equations are discretised after derivation. On the contrary, the discrete adjoint approach derives the adjoint equation after discretisation. There are three main issues on the use of continuous adjoints. (i) the adjoint equations are inconsistent, i.e., the stationary point of the primal does not coincide with a root in the adjoint gradient, (ii) the Eigen spectrum of the adjoint equations pose stability issues, and (iii) significant expertise is necessary to develop, maintain, and extend the solvers.

%In the discrete adjoint approach, Algorithmic Differentiation (AD) can be applied to the primal program code, differentiate it line by line and assemble the adjoint gradient using the chain-rule of calculus. Automatic software tools are available for AD like Tapenade[] and ADOL-C[]. In this work, we use source transformation AD approach, where the source code is differentiated and the differentiated source code is output by the AD tool. 
----